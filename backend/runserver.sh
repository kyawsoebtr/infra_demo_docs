while ! nc -w 1 -z ${DB_HOST} ${DB_PORT};
do sleep 5;
done;
python3 manage.py collectstatic --no-input
python manage.py runserver 0.0.0.0:8000