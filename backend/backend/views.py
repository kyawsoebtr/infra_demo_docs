from django.http import HttpResponse,HttpResponseForbidden


def my_protected_view(request):
    # Check if the user is authenticated
    if request.user.is_authenticated:
        return HttpResponse("This is a protected view and only accessible to logged-in users.")
    else:
        return HttpResponseForbidden("Unauthorized - You must be logged in to access this resource.")